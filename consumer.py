# Consumer.py
from kafka import KafkaConsumer
from kafka.errors import KafkaError

brokers = "localhost:9092"
topic = "test_topic"

# To consume latest messages and auto-commit offsets
consumer = KafkaConsumer(topic,
                         group_id='my-group',
                         bootstrap_servers=brokers)
print('Start consuming')

for message in consumer:
    print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                         message.offset, message.key,
                                         message.value))
