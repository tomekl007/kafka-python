./confluent start schema-registry

pyvenv myenv
source myenv/bin/activate
pip install kafka-python
python producer.py
python consumer.py