from confluent_kafka import Producer

p = Producer({'bootstrap.servers': 'localhost:9092'})
for data in [["key_1", "v_1"], ["key_2", "v_2"]]:
    p.produce('mytopic', data[1].encode('utf-8'), data[0].encode('utf-8'))
p.flush()
