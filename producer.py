# Producer.py

from kafka import KafkaProducer
from kafka.errors import KafkaError
import os, ssl

brokers = "localhost:9092"

topic = "test_topic"

producer = KafkaProducer(bootstrap_servers=brokers)

# Asynchronous by default
future = producer.send(topic, b'click_event', b'user_id_1')

# Block for 'synchronous' sends
try:
    record_metadata = future.get(timeout=10)
except KafkaError:
    pass

# Successful result returns assigned partition and offset
print(record_metadata.topic)
print(record_metadata.partition)
print(record_metadata.offset)
